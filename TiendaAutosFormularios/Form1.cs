﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TiendaAutos;

namespace TiendaAutosFormularios
{
    public partial class Form1 : Form
    {
        Tienda tienda;
        public Form1()
        {
            InitializeComponent();
            tienda = new Tienda();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string marca = textBox1.Text;
            string numeChasis = textBox2.Text;
            string modelo = textBox3.Text;
            int anio = Convert.ToInt32(textBox4.Text);
            double precio =  Convert.ToDouble(textBox5.Text);
            string desc = textBox6.Text;
            string color = textBox7.Text;
            tienda.AgregarAuto(marca,numeChasis, modelo, anio, precio, desc, color);
        }
    }
}
