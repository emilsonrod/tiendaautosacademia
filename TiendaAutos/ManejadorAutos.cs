﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiendaAutos
{
    public class ManejadorAutos
    {
        List<Auto> listaAutos;
        public ManejadorAutos()
        {
            listaAutos = new List<Auto>();
        }

        public bool AgregarAuto(string marca, string numeroChasis,
            string modelo, int anio, double precio,
            string descripcion, string color)
        {
            Auto autoIngresar = new Auto(marca, numeroChasis, modelo, anio, precio, 
                descripcion, color);
            listaAutos.Add(autoIngresar);
            return true;
        }
    }
}
