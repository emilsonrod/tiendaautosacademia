﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiendaAutos
{
    class Auto
    {
        private string marca;
        private string numeroChasis;
        private string modelo;
        private int anio;
        private double precio;
        private string descripcion;
        private string color;

        public Auto(string marca, string numeroChasis,
            string modelo, int anio, double precio,
            string descripcion, string color)
        {
            Marca = marca;
            this.numeroChasis = numeroChasis;
            Modelo = modelo;
            Anio = anio;
            Precio = precio;
            Descripcion = descripcion;
            Color = color;
        }
        public string Marca
        {
            get
            {
                return marca;
            }

            set
            {
                marca = value;
            }
        }        

        public string Modelo
        {
            get
            {
                return modelo;
            }

            set
            {
                modelo = value;
            }
        }

        public int Anio
        {
            get
            {
                return anio;
            }

            set
            {
                anio = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public string Color
        {
            get
            {
                return color;
            }

            set
            {
                color = value;
            }
        }        
    }
}
