﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiendaAutos
{
    public class Tienda
    {
        private ManejadorAutos manejadorAutos;
        private ManejadorClientes manejadorClientes;
        private ManejadorVentas manejadorVentas;
        private ManejadorProductos manejadorProductos;

        public Tienda()
        {
            manejadorAutos = new ManejadorAutos();
            manejadorClientes = new ManejadorClientes();
            manejadorVentas = new ManejadorVentas();
            manejadorProductos = new ManejadorProductos();
        }

        public bool AgregarAuto(string marca, string numeroChasis,
            string modelo, int anio, double precio,
            string descripcion, string color)
        {
            return manejadorAutos.AgregarAuto(marca, numeroChasis,
                modelo, anio, precio, descripcion, color);
        }
    }
}
